const HomePage = require("./home-page");

class LoginPage extends HomePage {
    get usernameTxt() {return $('//*[@id="username"]'); }
    get continueBtn() {return $('button[type="submit"]'); }
    get passwordTxt() {return $('//*[@id="password"]'); }
    get loginBtn() { return $('button[type="submit"]'); }
    get titleTxt() {return $('//*[@id="title-text"]'); }


   async enterUser (username) {
       await this.usernameTxt.setValue(username);
       await this.continueBtn.click();
   }

    async enterPassword(password) {
        await this.passwordTxt.setValue(password);
        await this.loginBtn.click();     
    }

    open () {
        return super.open('home');
    }
}

module.exports = new LoginPage();
