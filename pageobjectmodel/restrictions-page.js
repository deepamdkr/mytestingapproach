const HomePage = require("./home-page");

class RestrictionsPage extends HomePage {
    
    get usernameTxt() {return $('//*[@id="username"]'); }
    get continueBtn() {return $('button[type="submit"]'); }
    get passwordTxt() {return $('//*[@id="password"]'); }
    get loginBtn() { return $('button[type="submit"]'); }
    get titleTxt() {return $('//*[@id="title-text"]'); }
    get toolsmenuBtn() {return $('button[id="tools-menu-trigger"]'); }
    get restrictLink() {return $('button[id="bm-restrictions-link"]'); }
    get restrictMenu() {return $('h1*=Restrictions'); }
    get selectPermission() {return $('//*[@data-test-id="restrictions-dialog.content-mode-select"]'); }
    get anyEditOption() {return $('//*[@id="react-select-4-option-0"]'); }
    get someEditOption() {return $('//*[@id="react-select-4-option-1"]'); }
    get specificEditOption() {return $('//*[@id="react-select-4-option-2"]'); }
    get searchUser() {return $('//*[@data-test-id="user-and-group-search"]'); }
    get selectOption() {return $('//*[@data-test-id="user-and-group-search-default-permission"]'); }
    get addBtn() {return $('button*=Add'); }
    get selectUser1() { return $('//*[@id="react-select-restrictions:user-and-group-search:user-and-group-picker-option-0"]'); }
    get canViewBtn() {return $('//*[@id="react-select-6-option-0"]'); }
    get canEditBtn() {return $('//*[@id="react-select-6-option-1"]'); }
    get applyBtn() {return $('button*=Apply'); }
    get everyone() {return $('.css-jdn6mg=Everyone')}
    get selectDropdown() {return $('. css-5a7vsu-container')}
    get currentPermission() {return $('//*[@data-test-id="user-and-group-search-selector"]'); }
    get removeBtn() {return $('button*=Remove'); }
    get rowChange() {return $('//*[@data-test-id="rows-change-announcer"]'); }

   async enterUser (username) {
       await this.usernameTxt.setValue(username);
       await this.continueBtn.click();
   }

    async enterPassword(password) {
        await this.passwordTxt.setValue(password);
        await this.loginBtn.click();     
    }

    async accessRestriction() {
        await this.toolsmenuBtn.click();
        await browser.pause(2000);
        await this.restrictLink.click();
        await browser.pause(2000);
        this.restrictMenu.waitforDisaplayed();
        await browser.pause(2000);
    }

    async addAyoneCanEdit () {   
        await this.selectPermission.click();
        await this.anyEditOption.click();
        await this.applyBtn.click();
    }
    
    async addSomeCanEdit () {
        await this.selectPermission.click();
        await this.someEditOption.click();
        await this.searchUser.click();
        await this.selectUser1.click();
        await this.restrictMenu.click();
        await this.addBtn.click();
        await this.applyBtn.click();

    }

    async addViewOnlyRestriction () {
        await this.selectPermission.click();
        await this.specificEditOption.click();
        await this.searchUser.click();
        await this.selectUser1.click();
        await this.restrictMenu.click();
        await this.addBtn.click();
        await this.applyBtn.click();
        

    }

    

    open () {
        return super.open('spaces/ASSIGNMENT/overview');
    }

}
module.exports = new RestrictionsPage();

