const HomePage = require('../../pageobjectmodel/home-page');
const LoginPage = require('../../pageobjectmodel/login-page');
const RestrictionsPage = require('../../pageobjectmodel/restrictions-page');

describe('Open wiki space Assignment and click restriction' , () => {
    it('should be allowed to login', async () => {
        await RestrictionsPage.open();
        await RestrictionsPage.enterUser('deepamdkr@gmail.com');
        await browser.pause(2000);
        await RestrictionsPage.enterPassword('Qwerty123');
        await browser.pause(2000);
        await RestrictionsPage.accessRestriction();
    })
})


describe('Open wiki space Assignment and click restriction' , () => {
    it('should allow user to set access to anyone can edit', async () => {
/*        await RestrictionsPage.open();
        await RestrictionsPage.enterUser('deepamdkr@gmail.com');
        await browser.pause(2000);
        await RestrictionsPage.enterPassword('Qwerty123');
        await browser.pause(2000);
        await RestrictionsPage.accessRestriction();*/
        await expect(RestrictionsPage.restrictMenu).toBeExisting();
        await RestrictionsPage.addAyoneCanEdit();
    })
})



    describe('Open wiki space Assignment and click restriction' , () => {
    it('should allow user to set access to anyone can view and some edit', async () => {
 /*           await RestrictionsPage.open();
            await RestrictionsPage.enterUser('deepamdkr@gmail.com');
            await browser.pause(2000);
            await RestrictionsPage.enterPassword('Qwerty123');
            await browser.pause(2000);
            await RestrictionsPage.accessRestriction();*/
            await expect(RestrictionsPage.restrictMenu).toBeExisting();
            await RestrictionsPage.addSomeCanEdit();
            await expect(RestrictionsPage.everyone).toBeExisting();
        })
})


    describe('Open wiki space Assignment and click restriction' , () => {
        it('should allow user to set access to only some can view and  edit', async () => {
 /*           await RestrictionsPage.open();
            await RestrictionsPage.enterUser('deepamdkr@gmail.com');
            await browser.pause(2000);
            await RestrictionsPage.enterPassword('Qwerty123');
            await browser.pause(2000);
            await RestrictionsPage.accessRestriction();*/
            await expect(RestrictionsPage.restrictMenu).toBeExisting();
            await RestrictionsPage.addViewOnlyRestriction();
    })
})



